# Changelog

All notable changes to this project will be documented in this file.

## [0.1.26] - 2025-01-10

### 🚀 Features

- Added custom metrics example

### 🐛 Bug Fixes

- Log name of custom metrics specification file

### ⚙️ Miscellaneous Tasks

- Updated dependencies
- Updated version

## [0.1.25] - 2024-10-22

### 🐛 Bug Fixes

- Wrong default `api_url` in metrics.

## [0.1.24] - 2024-10-21

### 🚀 Features

- Added custom metrics
- Tag custom metrics with site name

## [0.1.23] - 2024-09-26

### Build

- Disabled warnings

## [0.1.9] - 2022-06-15

# 0.1.23
## Changes
Fluent Bit 3.1.8

# 0.1.22
## Changes
Fluent Bit 2.2.2

# 0.1.20
## Changes
Fluent Bit 2.1.9

# 0.1.19
## Changes
Fluent Bit 2.1.2
Updated dependencies

# 0.1.18
## Changes
Label cluster hostname for host metrics

# 0.1.17
## Changes
Updated to Fluent Bit 2.0.10
supd metrics

# 0.1.15
## Changes
Updated to Fluent Bit 2.0.6

# 0.1.14
## Changes
Updated to Fluent Bit 2.0.4

# 0.1.12
## Changes
Updated avassa-client.

# 0.1.11
## Changes
Updated to fluent bit 1.9.7
Fixed bug with consumer names, now every consumer gets a unique name.

# 0.1.9
## Changes
Updated to fluent bit 1.9.4
Fixed bug where reconnects happended too fast.

# 0.1.7

## Changes
If the environment variable `API_CA_CERT` is set, it will be used to verify the Edge Enforcer API endpoint.
Updated fluent bit to 1.9.3
Fixed label bugs in application metrics exporter
Only use gauges in metrics, counters doesn't work if application metrics are reset, e.g. at a restart.
Added host metrics exporter

<!-- Avassa 2024 -->
