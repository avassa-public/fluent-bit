# vi: ft=dockerfile
ARG FLUENT_BIT_VERSION
ARG FLUENT_BIT_TARGET_VERSION

FROM --platform=$TARGETPLATFORM fluent/fluent-bit:$FLUENT_BIT_VERSION-debug as builder
ARG TARGETPLATFORM
ARG BUILDPLATFORM

RUN apt-get update && \
  apt-get install -y cmake curl flex bison libclang-dev
RUN curl https://sh.rustup.rs -sSf | bash -s -- -y
ENV PATH="/root/.cargo/bin:${PATH}"

WORKDIR src
ADD fluent-bit-version fluent-bit-version
ADD in-avassa-logs in-avassa-logs
ADD in-avassa-metrics in-avassa-metrics
ADD in-avassa-common in-avassa-common
ADD Cargo* ./
ADD Makefile .
ADD wrapper.h .

RUN make all

# Create image from fluent-bit official image
FROM --platform=$TARGETPLATFORM fluent/fluent-bit:$FLUENT_BIT_TARGET_VERSION
ARG TARGETPLATFORM
ARG BUILDPLATFORM

COPY --from=builder /src/in-avassa-logs/flb-in_avassa_logs.so /avassa/flb-in_avassa_logs.so
COPY --from=builder /src/in-avassa-metrics/flb-in_avassa_metrics.so /avassa/flb-in_avassa_metrics.so


CMD ["/fluent-bit/bin/fluent-bit", "-e", "/avassa/flb-in_avassa_logs.so", "-e", "/avassa/flb-in_avassa_metrics.so", "-c", "/fluent-bit/etc/fluent-bit.conf"]
