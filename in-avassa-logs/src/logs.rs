use anyhow::Context;
use std::collections::HashMap;

pub(crate) struct LogCollectionArgs {
    pub cfg: crate::Config,
    pub msg_tx: std::sync::mpsc::SyncSender<super::LogMessage>,
    pub state_command_tx: tokio::sync::broadcast::Sender<super::StateCommand>,
}

pub(crate) unsafe extern "C" fn start_log_collection(args: *mut std::os::raw::c_void) {
    let args = &mut *(args as *mut LogCollectionArgs);
    let args = Box::from_raw(args);

    let rt = tokio::runtime::Builder::new_current_thread()
        .enable_all()
        .build()
        .expect("Start tokio runtime");
    if let Err(e) = rt.block_on(log_main(args.cfg, args.msg_tx, args.state_command_tx)) {
        eprintln!("{e:?}");
    }
}

async fn log_main(
    cfg: crate::Config,
    msg_tx: std::sync::mpsc::SyncSender<super::LogMessage>,
    state_command_tx: tokio::sync::broadcast::Sender<super::StateCommand>,
) -> anyhow::Result<()> {
    let ca_cert = std::env::var("API_CA_CERT").ok();
    let avassa_client = if let Some(ca_cert) = ca_cert {
        tracing::info!("API CA Certificate");
        avassa_client::Client::builder()
            .add_root_certificate(ca_cert.as_bytes())?
    } else {
        tracing::info!("No API CA Certificate");
        avassa_client::Client::builder()
            .danger_disable_cert_verification()
    };

    let avassa_client = match &cfg.credentials {
        in_avassa_common::AvassaCredentials::AppRoleId(approle_id) => avassa_client
            .application_login(&cfg.api_url, approle_id.as_deref())
            .await
            .context("application_login")?,
        in_avassa_common::AvassaCredentials::UsernamePwd(username, password) => avassa_client
            .login(&cfg.api_url, username, password)
            .await
            .context("username_login")?,
    };

    let site_name = avassa_client::utilities::state::site_name(&avassa_client)
        .await
        .context("get site_name")?;
    tracing::debug!("site name: {}", site_name);

    let mut log_check_interval = tokio::time::interval(std::time::Duration::from_secs(60));

    let mut active_logs: HashMap<String, tokio_util::sync::DropGuard> = HashMap::new();
    let mut log_id = 0;

    loop {
        log_check_interval.tick().await;

        let logs = get_volga_streams(&avassa_client, &cfg.application_re, &cfg.container_re)
            .await
            .context("get_volga_streams")?;

        active_logs.retain(|k, _| logs.contains(k));

        for log in logs {
            if active_logs.contains_key(&log) {
                continue;
            }
            tracing::info!("following log '{}'", log);
            let cancel_token = tokio_util::sync::CancellationToken::new();
            let child_token = cancel_token.child_token();

            let avassa_client = avassa_client.clone();
            let site_name = site_name.clone();
            let log_name = log.clone();
            let msg_tx = msg_tx.clone();
            let state_command_tx = state_command_tx.clone();
            let log_topic_id = log_id;
            let cfg = cfg.clone();
            log_id += 1;
            tracing::debug!("Starting log collection: {}", log_name);

            tokio::spawn(async move {
                let mut reconnect_interval =
                    tokio::time::interval(std::time::Duration::from_secs(30));
                reconnect_interval.set_missed_tick_behavior(tokio::time::MissedTickBehavior::Skip);
                loop {
                    reconnect_interval.tick().await;
                    if let Err(e) = monitor_logs(
                        &cfg,
                        log_name.clone(),
                        log_topic_id,
                        avassa_client.clone(),
                        site_name.clone(),
                        child_token.clone(),
                        msg_tx.clone(),
                        state_command_tx.subscribe(),
                    )
                    .await
                    {
                        tracing::error!("{} - {}", log_name, e);
                    }

                    if child_token.is_cancelled() {
                        break;
                    }
                }
            });

            active_logs.insert(log, cancel_token.drop_guard());
        }
    }
}

#[derive(Debug, serde::Serialize)]
struct LogMessage<'a, T> {
    #[serde(flatten)]
    inner: &'a avassa_client::volga::consumer::MessageMetadata<T>,
    #[serde(flatten)]
    labels: &'a std::collections::HashMap<String, String>,
}

#[allow(clippy::too_many_arguments)]
#[tracing::instrument(skip(avassa_client, cancel_token, msg_tx, config, state_command_rx))]
async fn monitor_logs(
    config: &super::Config,
    log_name: String,
    log_topic_id: u64,
    avassa_client: avassa_client::Client,
    site_name: String,
    cancel_token: tokio_util::sync::CancellationToken,
    msg_tx: std::sync::mpsc::SyncSender<super::LogMessage>,
    mut state_command_rx: tokio::sync::broadcast::Receiver<super::StateCommand>,
) -> anyhow::Result<()> {
    tracing::debug!("start monitoring");

    let mut labels = {
        // Get information about the log stream from volga
        let volga_state: avassa_client::utilities::types::state::volga::Topic = avassa_client
            .get_json(&format!("/v1/state/volga/topics/{log_name}"), None)
            .await
            .context("get volga topic metadata")?;
        volga_state.labels
    };

    // Add site_name as label
    labels.insert("site".to_string(), site_name.clone());

    let opts = avassa_client::volga::consumer::Options {
        position: avassa_client::volga::consumer::Position::Unread,
        ..Default::default()
    };

    let consumer_name = format!("fluent-bit-{log_name}");
    let mut consumer = avassa_client
        .volga_open_consumer(&consumer_name, &log_name, opts)
        .await
        .context("volga_open_consumer")?;

    loop {
        tokio::select! {
            _ = cancel_token.cancelled() => {
                tracing::info!("Cancelled");
                break
            },

            msg = consumer.consume::<String>() => match msg {
                Ok(mut volga_msg) => {
                    // Should we split logs on a separator?
                    if let Some(log_separator) = &config.split_logs_at {
                        let payload = volga_msg.payload;
                        for log in payload.split_terminator(log_separator) {
                            if !log.is_empty() {
                                volga_msg.payload = log.to_string();
                                encode_and_send_log(log_topic_id, &volga_msg, &labels, &msg_tx)?;
                            }
                        }
                    }
                    else {
                        encode_and_send_log(log_topic_id, &volga_msg,&labels, &msg_tx)?;
                    }
                },
                Err(e) => return Err(e.into()),
            },
            cmd = state_command_rx.recv() => match cmd {
                Ok(super::StateCommand::LogMessageAck { topic_id, seqno}) if topic_id == log_topic_id => {
                    tracing::trace!("ack {}", seqno);
                    consumer.ack(seqno).await?;
                },
                Ok(super::StateCommand::Pause) => {
                    tracing::info!("paused");
                    wait_for_resume(&mut state_command_rx, &mut consumer, log_topic_id).await?;
                },
                Ok(_) => (),
                Err(e) => {
                    return Err(e.into());
                }
            }
        }
    }

    Ok(())
}

fn encode_and_send_log(
    log_topic_id: u64,
    volga_msg: &avassa_client::volga::consumer::MessageMetadata<String>,
    labels: &HashMap<String, String>,
    msg_tx: &std::sync::mpsc::SyncSender<super::LogMessage>,
) -> anyhow::Result<()> {
    let msg = LogMessage {
        inner: volga_msg,
        labels,
    };

    tracing::trace!("LogMessage: {:?}", &msg);

    let mut mp = Vec::with_capacity(500 + msg.inner.payload.len());
    rmp::encode::write_array_len(&mut mp, 2).unwrap();

    // Encode time
    rmp::encode::write_ext_meta(&mut mp, 8, 0).unwrap();
    let now_sec = volga_msg.time.timestamp();
    let now_nsec = volga_msg.time.timestamp_subsec_nanos();
    mp.extend_from_slice(&(now_sec as u32).to_be_bytes());
    mp.extend_from_slice(&(now_nsec).to_be_bytes());
    rmp_serde::encode::write_named(&mut mp, &msg)?;
    let log_msg = super::LogMessage {
        topic_id: log_topic_id,
        seqno: volga_msg.seqno,
        msg: mp,
    };
    msg_tx.send(log_msg).context("mpx_tx.send")?;
    Ok(())
}

async fn wait_for_resume(
    state_command_rx: &mut tokio::sync::broadcast::Receiver<super::StateCommand>,
    consumer: &mut avassa_client::volga::consumer::Consumer,
    log_topic_id: u64,
) -> anyhow::Result<()> {
    loop {
        match state_command_rx.recv().await {
            Ok(super::StateCommand::Resume) => {
                tracing::info!("resuming");
                break;
            }
            Ok(super::StateCommand::LogMessageAck { topic_id, seqno })
                if topic_id == log_topic_id =>
            {
                tracing::trace!("ack {}", seqno);
                consumer.ack(seqno).await?;
            }
            _ => (),
        }
    }

    Ok(())
}

const LOG_STREAM_PREFIX: &str = "system:container-logs:";

async fn get_volga_streams(
    client: &avassa_client::Client,
    application_re: &regex::Regex,
    container_re: &regex::Regex,
) -> anyhow::Result<Vec<String>> {
    let json: Vec<avassa_client::utilities::types::state::volga::Topic> = client
        .get_json("/v1/state/volga/topics", None)
        .await
        .context("list volga topics on site")?;

    tracing::debug!(?json);

    let mut res = Vec::new();

    for topic in json {
        if !topic.name.starts_with(LOG_STREAM_PREFIX) {
            tracing::debug!("Skipping non application log {}", topic.name);
            continue;
        }

        match (
            topic.labels.get("application"),
            topic.labels.get("container-name"),
        ) {
            (Some(app_name), Some(container_name)) => {
                if application_re.is_match(app_name) && container_re.is_match(container_name) {
                    res.push(topic.name);
                } else {
                    tracing::debug!("Skipping topic {}", topic.name);
                }
            }
            _ => {
                tracing::debug!("Skipping topic {}", topic.name);
            }
        }
    }
    Ok(res)
}
