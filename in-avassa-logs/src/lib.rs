use std::os::raw::c_void;

pub use in_avassa_common::{bindings, Result};

mod logs;

struct FLBContext {
    log_msgs_rx: std::sync::mpsc::Receiver<LogMessage>,
    log_msgs_tx: std::sync::mpsc::SyncSender<LogMessage>,
    // We need to hold on to this, if it's dropped before the log collector starts, the tx won't
    // work
    _state_command_rx: tokio::sync::broadcast::Receiver<StateCommand>,
    state_command_tx: tokio::sync::broadcast::Sender<StateCommand>,
}

struct LogMessage {
    topic_id: u64,
    seqno: u64,
    msg: Vec<u8>,
}

#[derive(Clone)]
enum StateCommand {
    Pause,
    Resume,
    LogMessageAck { topic_id: u64, seqno: u64 },
}

#[derive(Clone, Debug)]
struct Config {
    interval_sec: i64,
    api_url: String,
    credentials: in_avassa_common::AvassaCredentials,
    log_queue_len: usize,
    application_re: regex::Regex,
    container_re: regex::Regex,
    split_logs_at: Option<String>,
}

macro_rules! check_result {
    ($exp: expr) => {
        match $exp {
            Ok(v) => v,
            Err(e) => {
                tracing::error!("{:?}", e.to_string());
                return -1;
            }
        }
    };
}

fn init_tracing(flb_input_instance: &bindings::flb_input_instance) -> Result<()> {
    let log_level = match flb_input_instance.log_level as bindings::flb_loglevel_helper {
        bindings::flb_loglevel_helper_LOG_LVL_OFF => tracing::level_filters::LevelFilter::OFF,
        bindings::flb_loglevel_helper_LOG_LVL_TRACE => tracing::level_filters::LevelFilter::TRACE,
        bindings::flb_loglevel_helper_LOG_LVL_DEBUG => tracing::level_filters::LevelFilter::DEBUG,
        bindings::flb_loglevel_helper_LOG_LVL_INFO => tracing::level_filters::LevelFilter::INFO,
        bindings::flb_loglevel_helper_LOG_LVL_WARN => tracing::level_filters::LevelFilter::WARN,
        bindings::flb_loglevel_helper_LOG_LVL_ERROR => tracing::level_filters::LevelFilter::ERROR,
        _ => unreachable!(),
    };
    in_avassa_common::init_logging(log_level)?;

    Ok(())
}

#[no_mangle]
unsafe extern "C" fn cb_init(
    flb_input_instance: *mut bindings::flb_input_instance,
    flb_config: *mut bindings::flb_config,
    _user: *mut std::os::raw::c_void,
) -> std::os::raw::c_int {
    if let Err(e) = init_tracing(&*flb_input_instance) {
        eprintln!("init_tracing: {e}");
        return -1;
    }
    tracing::debug!("cb_init");
    tracing::info!("in-avassa-logs version: {}", env!("CARGO_PKG_VERSION"));

    let cfg = check_result!(configure(flb_input_instance));

    let (log_msgs_tx, log_msgs_rx) = std::sync::mpsc::sync_channel(cfg.log_queue_len);
    let (state_command_tx, _state_command_rx) = tokio::sync::broadcast::channel(10);
    let ctx = Box::new(FLBContext {
        log_msgs_rx,
        log_msgs_tx,
        _state_command_rx,
        state_command_tx,
    });

    let args = Box::new(logs::LogCollectionArgs {
        cfg: cfg.clone(),
        msg_tx: ctx.log_msgs_tx.clone(),
        state_command_tx: ctx.state_command_tx.clone(),
    });
    let mut collector_tid: bindings::pthread_t = 0;

    let res = bindings::flb_worker_create(
        Some(logs::start_log_collection),
        Box::into_raw(args) as _,
        &mut collector_tid as _,
        flb_config,
    );

    if res != 0 {
        tracing::error!("Failed to create worker thread for metrics collection");
        return res;
    }

    let ctx_ptr = Box::into_raw(ctx) as *mut c_void;
    bindings::flb_input_set_context(flb_input_instance, ctx_ptr);

    bindings::flb_input_set_collector_time(
        flb_input_instance,
        Some(cb_collect),
        cfg.interval_sec,
        0,
        flb_config,
    );

    0
}

fn get_config_string_default(
    flb_input_instance: *mut bindings::flb_input_instance,
    key: &str,
    default: &str,
) -> Result<String> {
    let key = std::ffi::CString::new(key)?;
    let s = unsafe { bindings::flb_input_get_property(key.as_ptr(), flb_input_instance) };
    if !s.is_null() {
        let value = unsafe { std::ffi::CStr::from_ptr(s) };
        let value = value.to_str()?.parse()?;
        Ok(value)
    } else {
        Ok(default.to_string())
    }
}

fn get_config_string(
    flb_input_instance: *mut bindings::flb_input_instance,
    key: &str,
) -> Result<String> {
    let ckey = std::ffi::CString::new(key)?;
    let s = unsafe { bindings::flb_input_get_property(ckey.as_ptr(), flb_input_instance) };
    if !s.is_null() {
        let value = unsafe { std::ffi::CStr::from_ptr(s) };
        let value = value.to_str()?.parse()?;
        Ok(value)
    } else {
        Err(in_avassa_common::error(&format!("{key} not configured")))
    }
}

fn configure(flb_input_instance: *mut bindings::flb_input_instance) -> Result<Config> {
    let interval_sec =
        get_config_string_default(flb_input_instance, "interval_sec", "15")?.parse()?;
    let api_url = get_config_string_default(flb_input_instance, "api_url", "https://api:4646")?;
    let username = get_config_string_default(flb_input_instance, "username", "");
    let password = get_config_string(flb_input_instance, "password");

    let approle_id = get_config_string(flb_input_instance, "approle_id");

    let credentials = match (approle_id, (username, password)) {
        (Ok(approle_id), _) => in_avassa_common::AvassaCredentials::AppRoleId(Some(approle_id)),
        (_, (Ok(username), Ok(password))) => {
            in_avassa_common::AvassaCredentials::UsernamePwd(username, password)
        }
        _ => in_avassa_common::AvassaCredentials::AppRoleId(None),
    };

    let log_queue_len =
        get_config_string_default(flb_input_instance, "log_queue_len", "100")?.parse()?;

    let application_re = get_config_string(flb_input_instance, "application_re")?;
    let application_re = regex::Regex::new(&application_re)?;

    let container_re = get_config_string(flb_input_instance, "container_re")?;
    let container_re = regex::Regex::new(&container_re)?;

    let split_logs_at = get_config_string(flb_input_instance, "split_logs_at").ok();

    Ok(Config {
        interval_sec,
        api_url,
        credentials,
        log_queue_len,
        application_re,
        container_re,
        split_logs_at,
    })
}

/// # Safety
///
/// Assumes cb_init has been called and flb_input_instance is instantiated.
#[no_mangle]
pub unsafe extern "C" fn cb_collect(
    flb_input_instance: *mut bindings::flb_input_instance,
    _flb_config: *mut bindings::flb_config,
    ctx: *mut std::os::raw::c_void,
) -> std::os::raw::c_int {
    let ctx: &mut FLBContext = &mut *(ctx as *mut FLBContext);

    let mut seqnos = fnv::FnvHashMap::default();

    let mut retval = 0;
    loop {
        match ctx.log_msgs_rx.try_recv() {
            Ok(mut msg) => {
                let res = bindings::flb_input_log_append(
                    flb_input_instance,
                    std::ptr::null(),
                    0,
                    msg.msg.as_mut_ptr() as *const c_void,
                    msg.msg.len() as _,
                );
                if res == 0 {
                    seqnos.insert(msg.topic_id, msg.seqno);
                } else {
                    tracing::error!("Failed to store chunk");
                    retval = -1;
                    break;
                }
            }
            Err(std::sync::mpsc::TryRecvError::Empty) => {
                break;
            }
            Err(_) => {
                retval = -1;
                break;
            }
        }
    }

    for (topic_id, seqno) in seqnos {
        if let Err(e) = ctx
            .state_command_tx
            .send(StateCommand::LogMessageAck { topic_id, seqno })
        {
            tracing::trace!("send ack: {}", e);
            retval = -1;
        }
    }

    retval
}

/// # Safety
///
/// Assumes cb_init has been called
#[no_mangle]
pub unsafe extern "C" fn cb_pause(
    ctx: *mut std::os::raw::c_void,
    _flb_config: *mut bindings::flb_config,
) {
    tracing::info!("cb_pause");
    let ctx: &mut FLBContext = &mut *(ctx as *mut FLBContext);
    if let Err(e) = ctx.state_command_tx.send(StateCommand::Pause) {
        tracing::error!("cb_pause {}", e);
    }
}

/// # Safety
///
/// Assumes cb_init has been called
#[no_mangle]
pub unsafe extern "C" fn cb_resume(
    ctx: *mut std::os::raw::c_void,
    _flb_config: *mut bindings::flb_config,
) {
    tracing::info!("cb_resume");
    let ctx: &mut FLBContext = &mut *(ctx as *mut FLBContext);
    if let Err(e) = ctx.state_command_tx.send(StateCommand::Resume) {
        tracing::error!("cb_resume {}", e);
    }
}

#[no_mangle]
unsafe extern "C" fn cb_exit(
    ctx: *mut std::os::raw::c_void,
    _flb_config: *mut bindings::flb_config,
) -> std::os::raw::c_int {
    tracing::debug!("cb_exit");

    // Make sure we drop the CTX
    let unboxed_ctx: &mut FLBContext = &mut *(ctx as *mut FLBContext);
    drop(Box::from_raw(unboxed_ctx));
    0
}
