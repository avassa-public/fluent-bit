#include <fluent-bit.h>

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wformat-security"

void avassa_log_trace(const char* log) {
   flb_trace(log);
}

void avassa_log_debug(const char* log) {
   flb_debug(log);
}

void avassa_log_info(const char* log) {
   flb_info(log);
}

void avassa_log_warn(const char* log) {
   flb_warn(log);
}

void avassa_log_error(const char* log) {
   flb_error(log);
}
#pragma GCC diagnostic pop

extern int cb_init(struct flb_input_instance *, struct flb_config *, void *);
extern int cb_collect (struct flb_input_instance *, struct flb_config *, void *);
extern int cb_exit (void *, struct flb_config *);
extern void cb_pause(void* ctx, struct flb_config*);
extern void cb_resume(void* ctx, struct flb_config*);

static struct flb_config_map config_map[] = {
   {
    FLB_CONFIG_MAP_STR, "api_url", "https://api:4646",
    0, FLB_FALSE, 0,
    "URL of the Avassa API"
   },
   {
    FLB_CONFIG_MAP_STR, "username", "",
    0, FLB_FALSE, 0,
    "Avassa username"
   },
   {
    FLB_CONFIG_MAP_STR, "password", "",
    0, FLB_FALSE, 0,
    "Avassa password"
   },
   {
    FLB_CONFIG_MAP_STR, "approle_id", "",
    0, FLB_FALSE, 0,
    "Avassa approle id"
   },
   {
    FLB_CONFIG_MAP_INT, "interval_sec", "15",
    0, FLB_FALSE, 0,
    "Collect interval."
   },
   {
    FLB_CONFIG_MAP_INT, "log_queue_len", "100",
    0, FLB_FALSE, 0,
    "Internal log queue len. Logs are fetched asychnronously and placed in this queue, fluent bit will drain the queue at `interval_sec` periods."
   },
   {
    FLB_CONFIG_MAP_STR, "split_logs_at", NULL,
    0, FLB_FALSE, 0,
    "separator for splitting incoming logs into multiple"
   },
   {
    FLB_CONFIG_MAP_STR, "application_re", NULL,
    0, FLB_FALSE, 0,
    "Application inclusion regex"
   },
   {
    FLB_CONFIG_MAP_STR, "container_re", NULL,
    0, FLB_FALSE, 0,
    "Container inclusion regex"
   },
   {0}
};

extern struct flb_input_plugin in_avassa_logs_plugin;

struct flb_input_plugin in_avassa_logs_plugin = {
    .name         = "avassa_logs",
    .description  = "Avassa log and metrics",
    .cb_init      = cb_init,
    .cb_pre_run   = NULL,
    .cb_collect   = cb_collect,
    .cb_flush_buf = NULL,
    .config_map   = config_map,
    .cb_exit      = cb_exit,
    .cb_pause     = cb_pause,
    .cb_resume    = cb_resume,
};
