use std::{collections::BTreeMap, str::FromStr};

use anyhow::Context;

#[tracing::instrument(skip(avassa_client, msg_tx))]
pub(crate) async fn metrics_main(
    avassa_client: avassa_client::Client,
    msg_tx: std::sync::mpsc::SyncSender<crate::Metric>,
    custom_metrics_config_path: String,
) -> anyhow::Result<()> {
    let config = tokio::fs::read(&custom_metrics_config_path)
        .await
        .context(format!("Open {custom_metrics_config_path}"))?;

    let topics: Topics =
        serde_yaml::from_slice(&config).context(format!("Parsing {custom_metrics_config_path}"))?;

    tracing::info!("Successfully loaded custom metrics configuration");

    let site_name = avassa_client::utilities::state::site_name(&avassa_client)
        .await
        .context("Get site name")?;

    for mut topic in topics.topics {
        for metric in topic.metrics.iter_mut() {
            match jsonpath_rust::JsonPath::from_str(&metric.path) {
                Ok(jp) => metric.json_path = Some(jp),
                Err(e) => {
                    tracing::error!("Failed to parse {} for {}: {e}", metric.path, metric.name);
                    continue;
                }
            }
        }
        tokio::spawn(monitor_topic(
            avassa_client.clone(),
            site_name.clone(),
            msg_tx.clone(),
            topic,
        ));
    }

    std::future::pending::<()>().await;
    todo!()
}

#[tracing::instrument(skip_all, fields(topic=topic.name))]
async fn monitor_topic(
    client: avassa_client::Client,
    site_name: String,
    msg_tx: std::sync::mpsc::SyncSender<crate::Metric>,
    topic: Topic,
) {
    let mut interval = tokio::time::interval(std::time::Duration::from_secs(10));
    interval.set_missed_tick_behavior(tokio::time::MissedTickBehavior::Skip);
    let opts = avassa_client::volga::consumer::Options {
        position: avassa_client::volga::consumer::Position::End,
        ..Default::default()
    };
    let consumer_name = format!("in-avvassa_metrics-{}", topic.name);
    loop {
        interval.tick().await;

        let volga_open_consumer = client.volga_open_consumer(&consumer_name, &topic.name, opts);
        let mut consumer = match volga_open_consumer.await {
            Ok(c) => c,
            Err(e) => {
                tracing::error!("Failed to create consumer: {e}");
                continue;
            }
        };

        loop {
            match consumer.consume::<serde_json::Value>().await {
                Ok(msg) => {
                    for metric in &topic.metrics {
                        let m = metric.json_path.as_ref().unwrap().find_slice(&msg.payload);
                        let mut labels = metric.tags.clone();
                        labels.insert("site".to_owned(), site_name.clone());
                        for m in m {
                            let data = m.to_data();
                            if let Some(value) = data.as_f64() {
                                if let Err(e) = msg_tx.send(crate::Metric::Gauge {
                                    namespace: "".to_string(),
                                    subsystem: "".to_string(),
                                    name: metric.name.clone(),
                                    labels: labels.clone(),
                                    ts: chrono::Utc::now(),
                                    value,
                                }) {
                                    tracing::error!("Failed to send msg: {e}");
                                    break;
                                }
                            }
                        }
                    }
                }
                Err(e) => {
                    tracing::error!("Consumer error: {e}");
                    break;
                }
            }
        }
    }
}

#[derive(Debug, serde::Deserialize)]
struct Metric {
    name: String,
    // As in file
    path: String,
    // This is created as part of parsing
    #[serde(skip)]
    json_path: Option<jsonpath_rust::JsonPath>,
    #[serde(default)]
    tags: BTreeMap<String, String>,
}

#[derive(Debug, serde::Deserialize)]
struct Topic {
    name: String,
    metrics: Vec<Metric>,
}

#[derive(Debug, serde::Deserialize)]
struct Topics {
    topics: Vec<Topic>,
}
