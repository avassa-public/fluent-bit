use std::collections::BTreeMap;

pub(crate) async fn metrics_main(
    avassa_client: avassa_client::Client,
    msg_tx: std::sync::mpsc::SyncSender<crate::Metric>,
) -> anyhow::Result<()> {
    tracing::info!("starting host metrics collection");

    let mut interval = tokio::time::interval(std::time::Duration::from_secs(10));
    interval.set_missed_tick_behavior(tokio::time::MissedTickBehavior::Skip);
    loop {
        interval.tick().await;
        if let Err(e) = monitor_metrics(avassa_client.clone(), msg_tx.clone()).await {
            tracing::error!("monitor_metrics: {e}");
        }
    }
}

#[tracing::instrument(skip(avassa_client, msg_tx))]
async fn monitor_metrics(
    avassa_client: avassa_client::Client,
    msg_tx: std::sync::mpsc::SyncSender<crate::Metric>,
) -> anyhow::Result<()> {
    let opts = avassa_client::volga::consumer::Options {
        position: avassa_client::volga::consumer::Position::End,
        ..Default::default()
    };
    let mut consumer = avassa_client
        .volga_open_consumer("in-avassa_metrics-host", "system:host-metrics", opts)
        .await?;

    loop {
        match consumer.consume().await {
            Ok(msg) => {
                tracing::info!("host metrics update received");
                // tracing::debug!("msg: {msg:#?}");
                let metrics: avassa_client::utilities::host_metrics::Metrics =
                    serde_json::from_value(msg.payload)?;

                let labels: BTreeMap<String, String> = [
                    ("site".to_string(), metrics.site),
                    ("hostname".to_string(), metrics.hostname),
                    ("cluster_hostname".to_string(), metrics.cluster_hostname),
                ]
                .into_iter()
                .collect();

                // CPU metrics
                msg_tx.send(crate::Metric::Gauge {
                    namespace: "avassa".to_string(),
                    subsystem: "host".to_string(),
                    name: "vcpus".to_string(),
                    labels: labels.clone(),
                    ts: metrics.time,
                    value: metrics.cpu.vcpus as _,
                })?;

                let mut cpu_labels = labels.clone();
                for cpu in metrics.cpus {
                    cpu_labels.insert("cpu".to_string(), cpu.cpu);
                    msg_tx.send(crate::Metric::Gauge {
                        namespace: "avassa".to_string(),
                        subsystem: "host".to_string(),
                        name: "cpu_usr".to_string(),
                        labels: cpu_labels.clone(),
                        ts: metrics.time,
                        value: cpu.usr as _,
                    })?;

                    msg_tx.send(crate::Metric::Gauge {
                        namespace: "avassa".to_string(),
                        subsystem: "host".to_string(),
                        name: "cpu_sys".to_string(),
                        labels: cpu_labels.clone(),
                        ts: metrics.time,
                        value: cpu.sys as _,
                    })?;

                    msg_tx.send(crate::Metric::Gauge {
                        namespace: "avassa".to_string(),
                        subsystem: "host".to_string(),
                        name: "cpu_idle".to_string(),
                        labels: cpu_labels.clone(),
                        ts: metrics.time,
                        value: cpu.idle as _,
                    })?;
                }

                // Memory metrics
                msg_tx.send(crate::Metric::Gauge {
                    namespace: "avassa".to_string(),
                    subsystem: "host".to_string(),
                    name: "memory_total_bytes".to_string(),
                    labels: labels.clone(),
                    ts: metrics.time,
                    value: metrics.memory.total as _,
                })?;
                msg_tx.send(crate::Metric::Gauge {
                    namespace: "avassa".to_string(),
                    subsystem: "host".to_string(),
                    name: "memory_free_bytes".to_string(),
                    labels: labels.clone(),
                    ts: metrics.time,
                    value: metrics.memory.free as _,
                })?;
                msg_tx.send(crate::Metric::Gauge {
                    namespace: "avassa".to_string(),
                    subsystem: "host".to_string(),
                    name: "memory_available_bytes".to_string(),
                    labels: labels.clone(),
                    ts: metrics.time,
                    value: metrics.memory.available as _,
                })?;

                // Load Average
                msg_tx.send(crate::Metric::Gauge {
                    namespace: "avassa".to_string(),
                    subsystem: "host".to_string(),
                    name: "loadavg_1_minute".to_string(),
                    labels: labels.clone(),
                    ts: metrics.time,
                    value: metrics.loadavg.avg1 as _,
                })?;
                msg_tx.send(crate::Metric::Gauge {
                    namespace: "avassa".to_string(),
                    subsystem: "host".to_string(),
                    name: "loadavg_5_minute".to_string(),
                    labels: labels.clone(),
                    ts: metrics.time,
                    value: metrics.loadavg.avg5 as _,
                })?;
                msg_tx.send(crate::Metric::Gauge {
                    namespace: "avassa".to_string(),
                    subsystem: "host".to_string(),
                    name: "loadavg_15_minute".to_string(),
                    labels: labels.clone(),
                    ts: metrics.time,
                    value: metrics.loadavg.avg15 as _,
                })?;
                msg_tx.send(crate::Metric::Gauge {
                    namespace: "avassa".to_string(),
                    subsystem: "host".to_string(),
                    name: "loadavg_running".to_string(),
                    labels: labels.clone(),
                    ts: metrics.time,
                    value: metrics.loadavg.running as _,
                })?;
                msg_tx.send(crate::Metric::Gauge {
                    namespace: "avassa".to_string(),
                    subsystem: "host".to_string(),
                    name: "loadavg_total".to_string(),
                    labels: labels.clone(),
                    ts: metrics.time,
                    value: metrics.loadavg.total as _,
                })?;

                // Disk
                let mut disk_labels = labels.clone();
                for disk in metrics.disk {
                    disk_labels.insert("filesystem".to_string(), disk.filesystem);
                    disk_labels.insert("type".to_string(), disk.fs_type);
                    disk_labels.insert("mount".to_string(), disk.mount);
                    msg_tx.send(crate::Metric::Gauge {
                        namespace: "avassa".to_string(),
                        subsystem: "host".to_string(),
                        name: "disk_size_bytes".to_string(),
                        labels: disk_labels.clone(),
                        ts: metrics.time,
                        value: disk.size as _,
                    })?;
                    msg_tx.send(crate::Metric::Gauge {
                        namespace: "avassa".to_string(),
                        subsystem: "host".to_string(),
                        name: "disk_used_bytes".to_string(),
                        labels: disk_labels.clone(),
                        ts: metrics.time,
                        value: disk.used as _,
                    })?;
                    msg_tx.send(crate::Metric::Gauge {
                        namespace: "avassa".to_string(),
                        subsystem: "host".to_string(),
                        name: "disk_free_bytes".to_string(),
                        labels: disk_labels.clone(),
                        ts: metrics.time,
                        value: disk.free as _,
                    })?;
                    msg_tx.send(crate::Metric::Gauge {
                        namespace: "avassa".to_string(),
                        subsystem: "host".to_string(),
                        name: "disk_used_percent".to_string(),
                        labels: disk_labels.clone(),
                        ts: metrics.time,
                        value: disk.percentage_used as _,
                    })?;
                }
            }
            Err(e) => {
                return Err(e.into());
            }
        }
    }
}
