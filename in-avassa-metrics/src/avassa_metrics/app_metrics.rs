use std::collections::BTreeMap;

use anyhow::Context;

struct MetricsState {
    start_times_containers: std::collections::HashMap<(String, String, String), String>,
    start_times_applications: std::collections::HashMap<String, String>,
}

pub(crate) async fn metrics_main(
    avassa_client: avassa_client::Client,
    msg_tx: std::sync::mpsc::SyncSender<crate::Metric>,
) -> anyhow::Result<()> {
    let state = std::sync::Arc::new(tokio::sync::Mutex::new(MetricsState {
        start_times_containers: Default::default(),
        start_times_applications: Default::default(),
    }));

    let mut interval = tokio::time::interval(std::time::Duration::from_secs(10));
    interval.set_missed_tick_behavior(tokio::time::MissedTickBehavior::Skip);
    loop {
        interval.tick().await;
        if let Err(e) = monitor_metrics(avassa_client.clone(), msg_tx.clone(), state.clone()).await
        {
            tracing::error!("monitor_metrics error: {e}");
        }
    }
}
pub(crate) async fn metrics_main_supd(
    avassa_client: avassa_client::Client,
    msg_tx: std::sync::mpsc::SyncSender<crate::Metric>,
) -> anyhow::Result<()> {
    let state = std::sync::Arc::new(tokio::sync::Mutex::new(MetricsState {
        start_times_containers: Default::default(),
        start_times_applications: Default::default(),
    }));

    let mut interval = tokio::time::interval(std::time::Duration::from_secs(10));
    interval.set_missed_tick_behavior(tokio::time::MissedTickBehavior::Skip);
    loop {
        interval.tick().await;
        if let Err(e) =
            monitor_supd_metrics(avassa_client.clone(), msg_tx.clone(), state.clone()).await
        {
            tracing::error!("monitor_supd_metrics: {e}");
        }
    }
}

async fn monitor_metrics(
    avassa_client: avassa_client::Client,
    msg_tx: std::sync::mpsc::SyncSender<crate::Metric>,
    state: std::sync::Arc<tokio::sync::Mutex<MetricsState>>,
) -> anyhow::Result<()> {
    let opts = avassa_client::volga::consumer::Options {
        position: avassa_client::volga::consumer::Position::End,
        ..Default::default()
    };
    let mut consumer = avassa_client
        .volga_open_consumer("in-avassa_metrics-app", "system:application-metrics", opts)
        .await?;

    loop {
        match consumer.consume().await {
            Ok(msg) => {
                tracing::info!("application metrics update received");
                let metrics: avassa_client::utilities::application_metrics::Metrics =
                    serde_json::from_value(msg.payload)?;

                for entry in metrics.entries {
                    tracing::debug!(?entry);

                    if let Some(per_container) = entry.per_container {
                        let key = (
                            entry.application.clone(),
                            per_container.service_instance.clone(),
                            per_container.container.clone(),
                        );
                        let start_time = {
                            let mut state = state.lock().await;
                            if !state.start_times_containers.contains_key(&key) {
                                let svc_instances =
                                    avassa_client::utilities::state::service_instances(
                                        &avassa_client,
                                        &entry.application,
                                    )
                                    .await;

                                if let Ok(svc_instances) = svc_instances {
                                    for s in svc_instances {
                                        for c in s.containers {
                                            state.start_times_containers.insert(
                                                (entry.application.clone(), s.name.clone(), c.name),
                                                c.start_time.to_rfc3339(),
                                            );
                                        }
                                    }
                                }
                            }
                            state
                                .start_times_containers
                                .get(&key)
                                .ok_or_else(|| {
                                    anyhow::anyhow!(
                                        "Failed to get start_time for {}-{}-{}",
                                        key.0,
                                        key.1,
                                        key.2
                                    )
                                }).cloned()
                        };

                        let labels: BTreeMap<String, String> = [
                            ("site".to_string(), metrics.site.clone()),
                            ("application".to_string(), entry.application.clone()),
                            ("host".to_string(), entry.host),
                            ("service".to_string(), per_container.service_instance),
                            ("container".to_string(), per_container.container),
                            ("start_time".to_string(), start_time.unwrap_or_else(|_| "unknown".to_owned()))
                        ]
                        .into_iter()
                        .collect();

                        // CPU
                        msg_tx.send(crate::Metric::Gauge {
                            namespace: "avassa".to_string(),
                            subsystem: "container".to_string(),
                            name: "cpu_nanoseconds".to_string(),
                            labels: labels.clone(),
                            ts: entry.time,
                            value: per_container.cpu.nanoseconds as _,
                        })?;

                        msg_tx.send(crate::Metric::Gauge {
                            namespace: "avassa".to_string(),
                            subsystem: "container".to_string(),
                            name: "cpu_percent".to_string(),
                            labels: labels.clone(),
                            ts: entry.time,
                            value: per_container.cpu.percentage_used as _,
                        })?;

                        msg_tx.send(crate::Metric::Gauge {
                            namespace: "avassa".to_string(),
                            subsystem: "container".to_string(),
                            name: "cpus".to_string(),
                            labels: labels.clone(),
                            ts: entry.time,
                            value: per_container.cpu.cpus as _,
                        })?;

                        msg_tx.send(crate::Metric::Gauge {
                            namespace: "avassa".to_string(),
                            subsystem: "container".to_string(),
                            name: "cpu_shares".to_string(),
                            labels: labels.clone(),
                            ts: entry.time,
                            value: per_container.cpu.shares as _,
                        })?;

                        // Memory
                        msg_tx.send(crate::Metric::Gauge {
                            namespace: "avassa".to_string(),
                            subsystem: "container".to_string(),
                            name: "memory_bytes".to_string(),
                            labels: labels.clone(),
                            ts: entry.time,
                            value: per_container.memory.used as _,
                        })?;
                        msg_tx.send(crate::Metric::Gauge {
                            namespace: "avassa".to_string(),
                            subsystem: "container".to_string(),
                            name: "memory_total_bytes".to_string(),
                            labels: labels.clone(),
                            ts: entry.time,
                            value: per_container.memory.total as _,
                        })?;
                        msg_tx.send(crate::Metric::Gauge {
                            namespace: "avassa".to_string(),
                            subsystem: "container".to_string(),
                            name: "memory_used_percentage".to_string(),
                            labels: labels.clone(),
                            ts: entry.time,
                            value: per_container.memory.percentage_used as _,
                        })?;
                    } else if let Some(per_application) = entry.per_application {
                        let key = entry.application.clone();

                        let start_time = {
                            let mut state = state.lock().await;
                            if !state.start_times_applications.contains_key(&key) {
                                let application = avassa_client::utilities::state::application(
                                    &avassa_client,
                                    &entry.application,
                                )
                                .await
                                .context(format!("get start time for {}", entry.application))?;

                                state.start_times_applications.insert(
                                    key.clone(),
                                    application.config_modified_time.to_rfc3339(),
                                );
                            }
                            state
                                .start_times_applications
                                .get(&key)
                                .ok_or_else(|| {
                                    anyhow::anyhow!("Failed to get start_time for {key}",)
                                })?
                                .clone()
                        };

                        let labels: BTreeMap<String, String> = [
                            ("site".to_string(), metrics.site.clone()),
                            ("application".to_string(), entry.application.clone()),
                            ("host".to_string(), entry.host),
                            ("start_time".to_string(), start_time),
                        ]
                        .into_iter()
                        .collect();

                        if let Some(value) = per_application
                            .gateway_network
                            .as_ref()
                            .and_then(|gw| gw.tx_packets)
                        {
                            msg_tx.send(crate::Metric::Gauge {
                                namespace: "avassa".to_string(),
                                subsystem: "application".to_string(),
                                name: "tx_packets".to_string(),
                                labels: labels.clone(),
                                ts: entry.time,
                                value: value as _,
                            })?;
                        }
                        if let Some(value) = per_application
                            .gateway_network
                            .as_ref()
                            .and_then(|gw| gw.tx_bytes)
                        {
                            msg_tx.send(crate::Metric::Gauge {
                                namespace: "avassa".to_string(),
                                subsystem: "application".to_string(),
                                name: "tx_bytes".to_string(),
                                labels: labels.clone(),
                                ts: entry.time,
                                value: value as _,
                            })?;
                        }
                        if let Some(value) = per_application
                            .gateway_network
                            .as_ref()
                            .and_then(|gw| gw.rx_packets)
                        {
                            msg_tx.send(crate::Metric::Gauge {
                                namespace: "avassa".to_string(),
                                subsystem: "application".to_string(),
                                name: "rx_packets".to_string(),
                                labels: labels.clone(),
                                ts: entry.time,
                                value: value as _,
                            })?;
                        }
                        if let Some(value) = per_application
                            .gateway_network
                            .as_ref()
                            .and_then(|gw| gw.rx_bytes)
                        {
                            msg_tx.send(crate::Metric::Gauge {
                                namespace: "avassa".to_string(),
                                subsystem: "application".to_string(),
                                name: "rx_bytes".to_string(),
                                labels: labels.clone(),
                                ts: entry.time,
                                value: value as _,
                            })?;
                        }
                    }
                }
            }
            Err(e) => {
                tracing::error!("consume failed: {e}");
                return Err(e.into());
            }
        }
    }
}

async fn monitor_supd_metrics(
    avassa_client: avassa_client::Client,
    msg_tx: std::sync::mpsc::SyncSender<crate::Metric>,
    state: std::sync::Arc<tokio::sync::Mutex<MetricsState>>,
) -> anyhow::Result<()> {
    let opts = avassa_client::volga::consumer::Options {
        position: avassa_client::volga::consumer::Position::End,
        ..Default::default()
    };
    let mut consumer = avassa_client
        .volga_open_consumer("in-avassa_metrics-app", "system:supd-metrics", opts)
        .await?;

    loop {
        match consumer.consume().await {
            Ok(msg) => {
                tracing::info!("supd metrics update received");
                let metrics: avassa_client::utilities::supd_metrics::MetricEntry =
                    serde_json::from_value(msg.payload)?;

                let key = (
                    metrics.cluster_hostname.clone(),
                    metrics.cluster_hostname.clone(),
                    metrics.cluster_hostname.clone(),
                );

                let start_time = {
                    let mut state = state.lock().await;
                    if !state.start_times_containers.contains_key(&key) {
                        let cluster_hosts =
                            avassa_client::utilities::state::cluster_hosts(&avassa_client).await?;

                        for ch in cluster_hosts {
                            let key = (
                                ch.cluster_hostname.clone(),
                                ch.cluster_hostname.clone(),
                                ch.cluster_hostname.clone(),
                            );
                            state
                                .start_times_containers
                                .insert(key, ch.started_at.to_rfc3339());
                        }
                    }
                    state
                        .start_times_containers
                        .get(&key)
                        .ok_or_else(|| {
                            anyhow::anyhow!(
                                "Failed to get start_time for {}-{}-{}",
                                key.0,
                                key.1,
                                key.2
                            )
                        })?
                        .clone()
                };

                let labels: BTreeMap<String, String> = [
                    ("site".to_string(), metrics.site.clone()),
                    ("application".to_string(), "supd".to_string()),
                    ("host".to_string(), metrics.hostname),
                    ("service".to_string(), "supd".to_string()),
                    ("container".to_string(), "supd".to_string()),
                    ("start_time".to_string(), start_time),
                ]
                .into_iter()
                .collect();

                // CPU
                msg_tx.send(crate::Metric::Gauge {
                    namespace: "avassa".to_string(),
                    subsystem: "container".to_string(),
                    name: "cpu_nanoseconds".to_string(),
                    labels: labels.clone(),
                    ts: metrics.time,
                    value: metrics.cpu.nanoseconds as _,
                })?;

                msg_tx.send(crate::Metric::Gauge {
                    namespace: "avassa".to_string(),
                    subsystem: "container".to_string(),
                    name: "cpu_percent".to_string(),
                    labels: labels.clone(),
                    ts: metrics.time,
                    value: metrics.cpu.percentage_used as _,
                })?;

                msg_tx.send(crate::Metric::Gauge {
                    namespace: "avassa".to_string(),
                    subsystem: "container".to_string(),
                    name: "cpus".to_string(),
                    labels: labels.clone(),
                    ts: metrics.time,
                    value: metrics.cpu.cpus as _,
                })?;

                // Memory
                msg_tx.send(crate::Metric::Gauge {
                    namespace: "avassa".to_string(),
                    subsystem: "container".to_string(),
                    name: "memory_bytes".to_string(),
                    labels: labels.clone(),
                    ts: metrics.time,
                    value: metrics.memory.used as _,
                })?;
                msg_tx.send(crate::Metric::Gauge {
                    namespace: "avassa".to_string(),
                    subsystem: "container".to_string(),
                    name: "memory_total_bytes".to_string(),
                    labels: labels.clone(),
                    ts: metrics.time,
                    value: metrics.memory.total as _,
                })?;
                msg_tx.send(crate::Metric::Gauge {
                    namespace: "avassa".to_string(),
                    subsystem: "container".to_string(),
                    name: "memory_used_percentage".to_string(),
                    labels: labels.clone(),
                    ts: metrics.time,
                    value: metrics.memory.percentage_used as _,
                })?;
            }
            Err(e) => {
                return Err(e.into());
            }
        }
    }
}
