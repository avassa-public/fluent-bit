#include <fluent-bit.h>

extern int cb_init(struct flb_input_instance *, struct flb_config *, void *);
extern int cb_collect (struct flb_input_instance *, struct flb_config *, void *);
extern int cb_exit (void *, struct flb_config *);
/* extern void cb_pause(void* ctx, struct flb_config*); */
/* extern void cb_resume(void* ctx, struct flb_config*); */

static struct flb_config_map config_map[] = {
   {
    FLB_CONFIG_MAP_STR, "api_url", "https://localhost",
    0, FLB_FALSE, 0,
    "URL of the Avassa API"
   },
   {
    FLB_CONFIG_MAP_STR, "username", "",
    0, FLB_FALSE, 0,
    "Avassa username"
   },
   {
    FLB_CONFIG_MAP_STR, "password", "",
    0, FLB_FALSE, 0,
    "Avassa password"
   },
   {
    FLB_CONFIG_MAP_STR, "approle", "",
    0, FLB_FALSE, 0,
    "Avassa approle"
   },
   {
    FLB_CONFIG_MAP_INT, "interval_sec", "30",
    0, FLB_FALSE, 0,
    "Collect interval."
   },
   {0}
};

extern struct flb_input_plugin in_avassa_logs_plugin;

struct flb_input_plugin in_avassa_logs_plugin = {
    .name         = "avassa_logs",
    .description  = "Avassa log and metrics",
    .event_type   = FLB_INPUT_LOGS, //|FLB_INPUT_METRICS,
    .cb_init      = cb_init,
    .cb_pre_run   = NULL,
    .cb_collect   = cb_collect,
    .cb_flush_buf = NULL,
    .config_map   = config_map,
    .cb_exit      = cb_exit,
    /* .cb_pause     = cb_pause, */
    /* .cb_resume    = cb_resume, */
};
