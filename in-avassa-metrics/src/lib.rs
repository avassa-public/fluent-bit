use std::ffi::c_void;

use std::collections::BTreeMap;

mod cmetrics;

mod avassa_metrics;

pub use in_avassa_common::{bindings, Result};

mod config_bindings {
    #![allow(non_camel_case_types)]
    #![allow(dead_code)]
    #![allow(non_upper_case_globals)]
    #![allow(non_snake_case)]

    include!("config_bindings.rs");
}

#[derive(Debug)]
enum Metric {
    Gauge {
        namespace: String,
        subsystem: String,
        name: String,
        labels: BTreeMap<String, String>,
        ts: chrono::DateTime<chrono::Utc>,
        value: f64,
    },
}

struct FLBContext {
    metric_msgs_rx: std::sync::mpsc::Receiver<Metric>,
    metric_msgs_tx: std::sync::mpsc::SyncSender<Metric>,
}

#[derive(Clone, Debug)]
struct Config {
    interval_sec: i64,
    api_url: String,
    credentials: in_avassa_common::AvassaCredentials,
    enable_host_metrics: bool,
    enable_app_metrics: bool,
    enable_supd_metrics: bool,
    custom_metrics_config: Option<String>,
}

fn init_tracing(flb_input_instance: &bindings::flb_input_instance) -> Result<()> {
    let log_level = match flb_input_instance.log_level as bindings::flb_loglevel_helper {
        bindings::flb_loglevel_helper_LOG_LVL_OFF => tracing::level_filters::LevelFilter::OFF,
        bindings::flb_loglevel_helper_LOG_LVL_TRACE => tracing::level_filters::LevelFilter::TRACE,
        bindings::flb_loglevel_helper_LOG_LVL_DEBUG => tracing::level_filters::LevelFilter::DEBUG,
        bindings::flb_loglevel_helper_LOG_LVL_INFO => tracing::level_filters::LevelFilter::INFO,
        bindings::flb_loglevel_helper_LOG_LVL_WARN => tracing::level_filters::LevelFilter::WARN,
        bindings::flb_loglevel_helper_LOG_LVL_ERROR => tracing::level_filters::LevelFilter::ERROR,
        _ => unreachable!(),
    };

    in_avassa_common::init_logging(log_level)?;

    Ok(())
}

macro_rules! check_result {
    ($exp: expr) => {
        match $exp {
            Ok(v) => v,
            Err(e) => {
                eprintln!("{}", e.to_string());
                return -1;
            }
        }
    };
}

#[no_mangle]
unsafe extern "C" fn cb_init(
    flb_input_instance: *mut bindings::flb_input_instance,
    flb_config: *mut bindings::flb_config,
    _user: *mut std::os::raw::c_void,
) -> std::os::raw::c_int {
    if let Err(e) = init_tracing(&*flb_input_instance) {
        eprintln!("{e}");
        return -1;
    }
    tracing::debug!("cb_init");
    tracing::info!("in-avassa-metrics version: {}", env!("CARGO_PKG_VERSION"));

    let (metric_msgs_tx, metric_msgs_rx) = std::sync::mpsc::sync_channel(1000);

    let ctx = Box::new(FLBContext {
        metric_msgs_rx,
        metric_msgs_tx,
    });

    let cfg = check_result!(configure(flb_input_instance));

    tracing::debug!(?cfg);

    let args = Box::new(avassa_metrics::MetricsCollectionArgs {
        cfg: cfg.clone(),
        msg_tx: ctx.metric_msgs_tx.clone(),
    });

    let mut collector_tid: bindings::pthread_t = 0;

    let res = bindings::flb_worker_create(
        Some(avassa_metrics::start_metrics_collection),
        Box::into_raw(args) as _,
        &mut collector_tid as _,
        flb_config,
    );

    if res != 0 {
        tracing::error!("Failed to create worker thread for metrics collection");
        return res;
    }

    let ctx_ptr = Box::into_raw(ctx) as *mut c_void;
    bindings::flb_input_set_context(flb_input_instance, ctx_ptr);

    bindings::flb_input_set_collector_time(
        flb_input_instance,
        Some(cb_collect),
        cfg.interval_sec,
        0,
        flb_config,
    );
    0
}

fn get_config_string(
    flb_input_instance: *mut bindings::flb_input_instance,
    key: &str,
    default: &str,
) -> Result<String> {
    let key = std::ffi::CString::new(key)?;
    let s = unsafe { bindings::flb_input_get_property(key.as_ptr(), flb_input_instance) };
    if !s.is_null() {
        let value = unsafe { std::ffi::CStr::from_ptr(s) };
        let value = value.to_str()?.parse()?;
        Ok(value)
    } else {
        Ok(default.to_string())
    }
}

fn configure(flb_input_instance: *mut bindings::flb_input_instance) -> Result<Config> {
    let config: config_bindings::metric_config = unsafe {
        let mut config: std::mem::MaybeUninit<config_bindings::metric_config> =
            std::mem::MaybeUninit::uninit();

        bindings::flb_config_map_set(
            std::ptr::addr_of_mut!((*flb_input_instance).properties) as _,
            (*flb_input_instance).config_map,
            std::ptr::addr_of_mut!(config) as _,
        );

        config.assume_init()
    };

    let api_url = unsafe { std::ffi::CStr::from_ptr(config.api_url) }
        .to_str()?
        .to_string();
    let username = get_config_string(flb_input_instance, "username", "").map(|u| {
        if u.is_empty() {
            None
        } else {
            Some(u)
        }
    })?;
    let password = get_config_string(flb_input_instance, "password", "").map(|u| {
        if u.is_empty() {
            None
        } else {
            Some(u)
        }
    })?;
    let approle_id = get_config_string(flb_input_instance, "approle_id", "").map(|u| {
        if u.is_empty() {
            None
        } else {
            Some(u)
        }
    })?;

    let credentials = match (approle_id, (username, password)) {
        (Some(approle_id), _) => in_avassa_common::AvassaCredentials::AppRoleId(Some(approle_id)),
        (_, (Some(username), Some(password))) => {
            in_avassa_common::AvassaCredentials::UsernamePwd(username, password)
        }
        _ => in_avassa_common::AvassaCredentials::AppRoleId(None),
    };

    let custom_metrics_config = get_config_string(flb_input_instance, "custom_metrics_config", "")
        .map(|v| if v.is_empty() { None } else { Some(v) })?;

    Ok(Config {
        interval_sec: config.interval_sec as _,
        api_url,
        credentials,
        enable_host_metrics: config.enable_host_metrics,
        enable_app_metrics: config.enable_app_metrics,
        enable_supd_metrics: config.enable_supd_metrics,
        custom_metrics_config,
    })
}

fn update_metrics(ctx: &mut FLBContext) -> Result<cmetrics::CMetrics> {
    let mut cmt = cmetrics::CMetrics::new()?;

    loop {
        match ctx.metric_msgs_rx.try_recv() {
            Ok(metric) => match metric {
                Metric::Gauge {
                    namespace,
                    subsystem,
                    name,
                    labels,
                    value,
                    ts,
                    ..
                } => {
                    let (label_names, label_values): (Vec<_>, Vec<_>) =
                        labels.iter().map(|(k, v)| (k.as_str(), v.as_str())).unzip();
                    let key = cmetrics::key(&namespace, &subsystem, &name);
                    cmt.add_gauge(&key, "Gauge", &label_names)?;
                    cmt.set_gauge(&key, value, ts, &label_values)?;
                }
            },
            Err(std::sync::mpsc::TryRecvError::Empty) => {
                break;
            }
            Err(e) => return Err(Box::new(e)),
        }
    }

    Ok(cmt)
}

/// # Safety
///
/// Assumes cb_init has been called and flb_input_instance is instantiated.
#[no_mangle]
pub unsafe extern "C" fn cb_collect(
    flb_input_instance: *mut bindings::flb_input_instance,
    _flb_config: *mut bindings::flb_config,
    ctx: *mut std::os::raw::c_void,
) -> std::os::raw::c_int {
    let ctx: &mut FLBContext = &mut *(ctx as *mut FLBContext);

    tracing::debug!("cb_collect");
    match update_metrics(ctx) {
        Ok(mut cmt) => {
            let res = bindings::flb_input_metrics_append(
                flb_input_instance,
                std::ptr::null(),
                0,
                cmt.cmt(),
            );

            if res != 0 {
                tracing::error!("flb_input_metrics_append failed");
                return res;
            }
        }
        Err(e) => {
            tracing::error!(%e);
            return -1;
        }
    };

    0
}

#[no_mangle]
unsafe extern "C" fn cb_exit(
    ctx: *mut std::os::raw::c_void,
    _flb_config: *mut bindings::flb_config,
) -> std::os::raw::c_int {
    tracing::debug!("cb_exit");

    // Make sure we drop the CTX
    let unboxed_ctx: &mut FLBContext = &mut *(ctx as *mut FLBContext);
    drop(Box::from_raw(unboxed_ctx));
    0
}
