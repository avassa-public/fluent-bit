mod app_metrics;
mod custom_metrics;
mod host_metrics;

pub(crate) struct MetricsCollectionArgs {
    pub cfg: crate::Config,
    pub msg_tx: std::sync::mpsc::SyncSender<crate::Metric>,
}

pub(crate) unsafe extern "C" fn start_metrics_collection(args: *mut std::os::raw::c_void) {
    let args = &mut *(args as *mut MetricsCollectionArgs);
    let args = Box::from_raw(args);

    let rt = tokio::runtime::Builder::new_current_thread()
        .enable_all()
        .build()
        .expect("Start tokio runtime");

    if let Err(e) = rt.block_on(metrics_collectors(args.cfg, args.msg_tx)) {
        eprintln!("{e:?}");
    }
}

async fn metrics_collectors(
    cfg: crate::Config,
    msg_tx: std::sync::mpsc::SyncSender<crate::Metric>,
) -> anyhow::Result<()> {
    let ca_cert = std::env::var("API_CA_CERT").ok();
    let avassa_client = if let Some(ca_cert) = ca_cert {
        tracing::info!("API CA Certificate");
        avassa_client::Client::builder().add_root_certificate(ca_cert.as_bytes())?
    } else {
        tracing::info!("No API CA Certificate");
        avassa_client::Client::builder().danger_disable_cert_verification()
    };

    let avassa_client = match cfg.credentials {
        in_avassa_common::AvassaCredentials::AppRoleId(approle_id) => {
            avassa_client
                .application_login(&cfg.api_url, approle_id.as_deref())
                .await?
        }
        in_avassa_common::AvassaCredentials::UsernamePwd(username, password) => {
            avassa_client
                .login(&cfg.api_url, &username, &password)
                .await?
        }
    };

    let app = if cfg.enable_app_metrics {
        tokio::spawn(app_metrics::metrics_main(
            avassa_client.clone(),
            msg_tx.clone(),
        ))
    } else {
        tokio::spawn(std::future::pending())
    };

    let host = if cfg.enable_host_metrics {
        tokio::spawn(host_metrics::metrics_main(
            avassa_client.clone(),
            msg_tx.clone(),
        ))
    } else {
        tokio::spawn(std::future::pending())
    };

    let supd = if cfg.enable_supd_metrics {
        tokio::spawn(app_metrics::metrics_main_supd(
            avassa_client.clone(),
            msg_tx.clone(),
        ))
    } else {
        tokio::spawn(std::future::pending())
    };

    let custom = if let Some(custom_metrics_config) = &cfg.custom_metrics_config {
        tokio::spawn(custom_metrics::metrics_main(
            avassa_client,
            msg_tx.clone(),
            custom_metrics_config.to_owned(),
        ))
    } else {
        tokio::spawn(std::future::pending())
    };

    tokio::select! {
        app_res = app => app_res?,
        host_res = host => host_res?,
        supd_res = supd => supd_res?,
        custom_res = custom => custom_res?,
    }
}
