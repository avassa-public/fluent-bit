use std::collections::HashMap;

pub(crate) struct CMetrics {
    cmt: *mut crate::bindings::cmt,
    // counters: HashMap<Key, *mut crate::bindings::cmt_counter>,
    gauges: HashMap<Key, *mut crate::bindings::cmt_gauge>,
}

#[derive(Clone, Debug, Hash, PartialEq, Eq)]
pub(crate) struct Key(String, String, String);

pub(crate) fn key(namespace: &str, subsystem: &str, name: &str) -> Key {
    Key(
        namespace.to_string(),
        subsystem.to_string(),
        name.to_string(),
    )
}

impl From<(&str, &str, &str)> for Key {
    fn from(k: (&str, &str, &str)) -> Self {
        Self(k.0.to_string(), k.1.to_string(), k.2.to_string())
    }
}

//impl ToString for Key {
//    fn to_string(&self) -> String {
//        format!("{}:{}:{}", self.0, self.1, self.2)
//    }
//}

impl std::fmt::Display for Key {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}:{}:{}", self.0, self.1, self.2)
    }
}

impl CMetrics {
    pub(crate) fn new() -> crate::Result<Self> {
        let cmt = unsafe { crate::bindings::cmt_create() };

        if !cmt.is_null() {
            Ok(Self {
                cmt,
                // counters: HashMap::new(),
                gauges: HashMap::new(),
            })
        } else {
            Err(in_avassa_common::error("cmt_create failed"))
        }
    }

    pub fn cmt(&mut self) -> *mut crate::bindings::cmt {
        self.cmt
    }

    // pub fn add_counter(&mut self, key: &Key, help: &str, labels: &[&str]) -> crate::Result<()> {
    //     if self.counters.contains_key(key) {
    //         return Ok(());
    //     }

    //     let cnamespace = std::ffi::CString::new(key.0.clone())?;
    //     let csubsystem = std::ffi::CString::new(key.1.clone())?;
    //     let cname = std::ffi::CString::new(key.2.clone())?;
    //     let chelp = std::ffi::CString::new(help)?;

    //     let mut clabels: Vec<*mut std::os::raw::c_char> = Vec::with_capacity(labels.len());

    //     for label in labels {
    //         clabels.push(std::ffi::CString::new(*label)?.into_raw());
    //     }

    //     let counter = unsafe {
    //         crate::bindings::cmt_counter_create(
    //             self.cmt,
    //             cnamespace.into_raw(),
    //             csubsystem.into_raw(),
    //             cname.into_raw(),
    //             chelp.into_raw(),
    //             clabels.len() as _,
    //             clabels.as_mut_ptr(),
    //         )
    //     };

    //     if counter.is_null() {
    //         return Err(in_avassa_common::error(&format!(
    //             "Failed to create counter {}",
    //             key.to_string()
    //         )));
    //     }

    //     self.counters.insert(key.clone(), counter);
    //     Ok(())
    // }

    pub fn add_gauge(&mut self, key: &Key, help: &str, labels: &[&str]) -> crate::Result<()> {
        if self.gauges.contains_key(key) {
            return Ok(());
        }

        tracing::debug!("add_gauge: {}", key.to_string());

        let gauge = unsafe {
            let mut clabels: Vec<*mut std::os::raw::c_char> = Vec::with_capacity(labels.len());

            for label in labels {
                clabels.push(std::ffi::CString::new(*label)?.into_raw());
            }

            let cnamespace = std::ffi::CString::new(key.0.clone())?;
            let csubsystem = std::ffi::CString::new(key.1.clone())?;
            let cname = std::ffi::CString::new(key.2.clone())?;
            let chelp = std::ffi::CString::new(help)?;

            let cnamespace = cnamespace.into_raw();
            let csubsystem = csubsystem.into_raw();
            let cname = cname.into_raw();
            let chelp = chelp.into_raw();

            let gauge = crate::bindings::cmt_gauge_create(
                self.cmt,
                cnamespace,
                csubsystem,
                cname,
                chelp,
                clabels.len() as _,
                clabels.as_mut_ptr(),
            );

            // Free memory
            drop(std::ffi::CString::from_raw(cnamespace));
            drop(std::ffi::CString::from_raw(csubsystem));
            drop(std::ffi::CString::from_raw(cname));
            drop(std::ffi::CString::from_raw(chelp));
            for clabel in clabels {
                drop(std::ffi::CString::from_raw(clabel));
            }

            gauge
        };

        if gauge.is_null() {
            return Err(in_avassa_common::error(&format!(
                "Failed to create gauge {}",
                key
            )));
        }

        self.gauges.insert(key.clone(), gauge);
        Ok(())
    }

    // fn remove_counter(&mut self, key: Key) -> crate::Result<()> {
    //     if let Some(ctr) = self.counters.remove(&key) {
    //         if unsafe { crate::bindings::cmt_counter_destroy(ctr) } != 0 {
    //             return Err(in_avassa_common::error(&format!(
    //                 "Failed to remove counter: {}",
    //                 key.to_string()
    //             )));
    //         }
    //     }
    //     Ok(())
    // }
    //

    // pub(crate) fn flush_all_metrics(&mut self) -> anyhow::Result<()> {
    //     let mut res = Ok(());
    //     for (key, g) in self.gauges.drain() {
    //         tracing::info!("remove_gauge: {}", key.to_string());
    //         if unsafe { crate::bindings::cmt_gauge_destroy(g) } != 0 {
    //             res = Err(anyhow::anyhow!(
    //                 "Failed to remove counter: {}",
    //                 key.to_string()
    //             ));
    //         }
    //     }
    //     res
    // }

    // pub fn ctrs_keep_only(
    //     &mut self,
    //     keys_to_keep: &std::collections::HashSet<Key>,
    // ) -> crate::Result<()> {
    //     let ctrs_to_remove: Vec<Key> = self
    //         .counters
    //         .keys()
    //         .filter(|k| !keys_to_keep.contains(k))
    //         .cloned()
    //         .collect();
    //     for key in ctrs_to_remove {
    //         self.remove_counter(key)?;
    //     }

    //     Ok(())
    // }

    // pub fn set_counter(
    //     &mut self,
    //     key: &Key,
    //     value: f64,
    //     timestamp: chrono::DateTime<chrono::Utc>,
    //     labels: &[&str],
    // ) -> crate::Result<()> {
    //     let counter = self.counters.get(key).ok_or_else(|| {
    //         in_avassa_common::error(&format!("No counter for {}", key.to_string()))
    //     })?;

    //     let mut clabels: Vec<*mut std::os::raw::c_char> = Vec::with_capacity(labels.len());

    //     for label in labels {
    //         clabels.push(std::ffi::CString::new(*label)?.into_raw());
    //     }

    //     let res = unsafe {
    //         // int cmt_counter_set(struct cmt_counter *counter, uint64_t timestamp, double val,
    //         //              int labels_count, char **label_vals);
    //         crate::bindings::cmt_counter_set(
    //             *counter,
    //             timestamp.timestamp_nanos() as _,
    //             value,
    //             labels.len() as _,
    //             clabels.as_mut_ptr(),
    //         )
    //     };

    //     if res != 0 {
    //         return Err(in_avassa_common::error(&format!(
    //             "Failed to set counter {}",
    //             key.to_string()
    //         )));
    //     }

    //     Ok(())
    // }

    pub fn set_gauge(
        &mut self,
        key: &Key,
        value: f64,
        timestamp: chrono::DateTime<chrono::Utc>,
        labels: &[&str],
    ) -> crate::Result<()> {
        let gauge = self.gauges.get(key).ok_or_else(|| {
            in_avassa_common::error(&format!("No counter for {}", key))
        })?;

        let mut clabels: Vec<*mut std::os::raw::c_char> = Vec::with_capacity(labels.len());

        for label in labels {
            clabels.push(std::ffi::CString::new(*label)?.into_raw());
        }

        let res = unsafe {
            // int cmt_counter_set(struct cmt_counter *counter, uint64_t timestamp, double val,
            //              int labels_count, char **label_vals);
            crate::bindings::cmt_gauge_set(
                *gauge,
                timestamp
                    .timestamp_nanos_opt()
                    .ok_or(anyhow::anyhow!("Failed to get timestamp nanos"))? as _,
                value,
                labels.len() as _,
                clabels.as_mut_ptr(),
            )
        };

        unsafe {
            for clabel in clabels {
                drop(std::ffi::CString::from_raw(clabel));
            }
        }

        if res != 0 {
            return Err(in_avassa_common::error(&format!(
                "Failed to set gauge {}",
                key
            )));
        }

        Ok(())
    }
}

impl Drop for CMetrics {
    fn drop(&mut self) {
        unsafe {
            crate::bindings::cmt_destroy(self.cmt);
        }
    }
}
