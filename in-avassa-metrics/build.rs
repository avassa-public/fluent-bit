use bindgen::builder;

fn main() -> anyhow::Result<()> {
    let bindings = builder()
        .clang_arg("-I../fluent-bit/include")
        .header("csrc/config.h")
        .generate()
        .expect("Failed to generate bindings");

    bindings.write_to_file("src/config_bindings.rs")?;

    Ok(())
}
