#pragma once
#include <stdint.h>
#include <stdbool.h>

struct metric_config {
  int32_t interval_sec;
  const char* api_url;
  bool enable_app_metrics;
  bool enable_host_metrics;
  bool enable_supd_metrics;
};
