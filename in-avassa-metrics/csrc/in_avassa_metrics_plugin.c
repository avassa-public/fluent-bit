#include <fluent-bit.h>
#include "config.h"

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wformat-security"
void avassa_log_trace(const char* log) {
   flb_trace(log);
}

void avassa_log_debug(const char* log) {
   /* fprintf(stderr, "\n\navassa_log_debug: %d\n\n", flb_log_check(FLB_LOG_INFO)); */
   flb_debug(log);
}

void avassa_log_info(const char* log) {
   flb_info(log);
}

void avassa_log_warn(const char* log) {
   flb_warn(log);
}

void avassa_log_error(const char* log) {
   flb_error(log);
}
#pragma GCC diagnostic pop

extern int cb_init(struct flb_input_instance *, struct flb_config *, void *);
extern int cb_collect (struct flb_input_instance *, struct flb_config *, void *);
extern int cb_exit (void *, struct flb_config *);

static struct flb_config_map config_map[] = {
   {
    FLB_CONFIG_MAP_STR, "api_url", "https://api:4646",
    0, FLB_TRUE, offsetof(struct metric_config, api_url),
    "URL of the Avassa API"
   },
   {
    FLB_CONFIG_MAP_STR, "username", "",
    0, FLB_FALSE, 0,
    "Avassa username"
   },
   {
    FLB_CONFIG_MAP_STR, "password", "",
    0, FLB_FALSE, 0,
    "Avassa password"
   },
   {
    FLB_CONFIG_MAP_STR, "approle_id", "",
    0, FLB_FALSE, 0,
    "Avassa approle"
   },
   {
    FLB_CONFIG_MAP_INT, "interval_sec", "30",
    0, FLB_TRUE, offsetof(struct metric_config, interval_sec),
    "Collect interval."
   },
   {
     FLB_CONFIG_MAP_BOOL, "enable_app_metrics", "true",
     0, FLB_TRUE, offsetof(struct metric_config, enable_app_metrics),
     "Enable application metrics."
   },
   {
     FLB_CONFIG_MAP_BOOL, "enable_host_metrics", "false",
     0, FLB_TRUE, offsetof(struct metric_config, enable_host_metrics),
     "Enable host metrics."
   },
   {
     FLB_CONFIG_MAP_BOOL, "enable_supd_metrics", "false",
     0, FLB_TRUE, offsetof(struct metric_config, enable_supd_metrics),
     "Enable supd metrics."
   },
   {
    FLB_CONFIG_MAP_STR, "custom_metrics_config", "",
    0, FLB_FALSE, 0,
    "Custom metrics configuration"
   },
   {0}
};

extern struct flb_input_plugin in_avassa_metrics_plugin;

struct flb_input_plugin in_avassa_metrics_plugin = {
    .name         = "avassa_metrics",
    .description  = "Avassa metrics",
    .cb_init      = cb_init,
    .cb_pre_run   = NULL,
    .cb_collect   = cb_collect,
    .cb_flush_buf = NULL,
    .config_map   = config_map,
    .cb_exit      = cb_exit,
};
