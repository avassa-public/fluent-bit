use bindgen::builder;

fn main() -> anyhow::Result<()> {
    let bindings = builder()
        .clang_arg("-I../fluent-bit/lib/cfl/lib/xxhash")
        .clang_arg("-I../fluent-bit/lib/cfl/include")
        .clang_arg("-I../fluent-bit/lib/cmetrics/include")
        .clang_arg("-I../fluent-bit/lib/c-ares-1.34.4/include")
        .clang_arg("-I../fluent-bit/lib/flb_libco")
        .clang_arg("-I../fluent-bit/lib/msgpack-c/include")
        .clang_arg("-I../fluent-bit/build/lib/monkey/include/monkey")
        .clang_arg("-I../fluent-bit/lib/monkey/include")
        .clang_arg("-I../fluent-bit/lib/ctraces/include")
        .clang_arg("-I../fluent-bit/lib/ctraces/lib/mpack/src")
        .clang_arg("-I../fluent-bit/include")
        .blocklist_item("IPPORT_RESERVED")
        .allowlist_type("flb_input_plugin")
        .allowlist_type("flb_loglevel_helper")
        .allowlist_function("flb_config_map_set")
        .allowlist_function("flb_input_set_context")
        .allowlist_function("flb_input_set_collector_time")
        .allowlist_function("flb_input_get_property")
        .allowlist_function("flb_input_log_append")
        .allowlist_function("flb_input_metrics_append")
        .allowlist_function("flb_worker_create")
        .allowlist_function("cmt_create")
        .allowlist_function("cmt_destroy")
        .allowlist_function("cmt_gauge_set")
        .allowlist_function("cmt_counter_create")
        .allowlist_function("cmt_counter_set")
        .allowlist_function("cmt_counter_destroy")
        .allowlist_function("cmt_gauge_destroy")
        .allowlist_function("cmt_gauge_create")
        .no_debug("flb_sds")
        .no_debug("cmt_sds")
        .header("../wrapper.h")
        .generate()
        .expect("Failed to generate bindings");

    bindings.write_to_file("src/bindings.rs")?;

    Ok(())
}
