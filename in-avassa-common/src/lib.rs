pub mod bindings {
    #![allow(non_camel_case_types)]
    #![allow(dead_code)]
    #![allow(non_upper_case_globals)]
    #![allow(non_snake_case)]
    include!("bindings.rs");
}

pub type Result<T> = std::result::Result<T, Box<dyn std::error::Error + Send + Sync>>;

pub fn init_logging(level: tracing::level_filters::LevelFilter) -> Result<()> {
    tracing_subscriber::fmt()
        .without_time()
        .with_ansi(false)
        .with_writer(FBMakeWriter {})
        .with_max_level(level)
        .with_level(false)
        .try_init()
}

extern "C" {
    fn avassa_log_debug(log: *const std::os::raw::c_char);
    fn avassa_log_trace(log: *const std::os::raw::c_char);
    fn avassa_log_info(log: *const std::os::raw::c_char);
    fn avassa_log_warn(log: *const std::os::raw::c_char);
    fn avassa_log_error(log: *const std::os::raw::c_char);
}

struct FBMakeWriter {}

enum FBWriter {
    Trace,
    Debug,
    Info,
    Warn,
    Error,
}

impl std::io::Write for FBWriter {
    fn write(&mut self, buf: &[u8]) -> std::io::Result<usize> {
        let b: Vec<_> = buf
            .iter()
            .map(|u| unsafe { std::num::NonZeroU8::new_unchecked(*u) })
            .collect();
        let c_log = std::ffi::CString::from(b);
        match self {
            Self::Trace => unsafe {
                avassa_log_trace(c_log.as_ptr());
            },
            Self::Debug => unsafe {
                avassa_log_debug(c_log.as_ptr());
            },
            Self::Info => unsafe {
                avassa_log_info(c_log.as_ptr());
            },
            Self::Warn => unsafe {
                avassa_log_warn(c_log.as_ptr());
            },
            Self::Error => unsafe {
                avassa_log_error(c_log.as_ptr());
            },
        };
        Ok(buf.len())
    }

    fn flush(&mut self) -> std::io::Result<()> {
        todo!()
    }
}

impl<'a> tracing_subscriber::fmt::MakeWriter<'a> for FBMakeWriter {
    type Writer = FBWriter;

    fn make_writer(&'a self) -> Self::Writer {
        FBWriter::Error
    }

    fn make_writer_for(&'a self, meta: &tracing::metadata::Metadata<'_>) -> Self::Writer {
        // eprintln!("{:#?}", meta);
        let _ = meta;
        // self.make_writer()
        match *meta.level() {
            tracing::Level::TRACE => FBWriter::Trace,
            tracing::Level::DEBUG => FBWriter::Debug,
            tracing::Level::INFO => FBWriter::Info,
            tracing::Level::WARN => FBWriter::Warn,
            tracing::Level::ERROR => FBWriter::Error,
        }
    }
}

#[derive(Debug)]
pub struct PluginError(String);

impl std::fmt::Display for PluginError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.0)
    }
}

impl std::error::Error for PluginError {}
unsafe impl Send for PluginError {}
unsafe impl Sync for PluginError {}

pub fn error(msg: &str) -> Box<PluginError> {
    Box::new(PluginError(msg.to_string()))
}

#[derive(Clone)]
pub enum AvassaCredentials {
    UsernamePwd(String, String),
    AppRoleId(Option<String>),
}

impl std::fmt::Debug for AvassaCredentials {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::UsernamePwd(_, _) => write!(f, "UsernamePwd"),
            Self::AppRoleId(_) => write!(f, "AppRoleId"),
        }
    }
}
