CFLAGS += -m64 -O2
LDFLAGS += -shared

FLUENT_BIT_VERSION=$(shell cat fluent-bit-version)
FLUENT_BIT_BIN = fluent-bit/build/bin/fluent-bit

fluent-bit:
	mkdir -p $@
	curl -L "https://github.com/fluent/fluent-bit/archive/refs/tags/v$(FLUENT_BIT_VERSION).tar.gz" | tar xfz - -C $@ --strip-components=1
	cd fluent-bit/build && cmake .. -DCMAKE_BUILD_TYPE=RELEASE -DFLB_CONFIG_YAML=Off

$(FLUENT_BIT_BIN): fluent-bit
	cd fluent-bit/build && make fluent-bit-bin

in-avassa-logs/flb-in_avassa_logs.so:
	make -C in-avassa-logs

in-avassa-metrics/flb-in_avassa_metrics.so:
	make -C in-avassa-metrics

.PHONY: all all-debug clean real-clean run image
all: fluent-bit
	make -C in-avassa-metrics all
	make -C in-avassa-logs all

all-debug: fluent-bit
	make -C in-avassa-metrics all-debug
	make -C in-avassa-logs all-debug

clean:
	make -C in-avassa-logs clean
	make -C in-avassa-metrics clean

real-clean: clean
	cargo clean
	rm -rf fluent-bit

run: $(FLUENT_BIT_BIN)
	# fluent-bit/build/bin/fluent-bit -vv -e ./flb-in_avassa.so -i avassa -o stdout
	# AWS_PROFILE=customer-demos $(FLUENT_BIT_BIN) -c fluent-bit.conf
	$(FLUENT_BIT_BIN) -c fluent-bit.conf

run-valgrind: $(FLUENT_BIT_BIN)
	# fluent-bit/build/bin/fluent-bit -vv -e ./flb-in_avassa.so -i avassa -o stdout
	valgrind --leak-check=full --log-file="valgrind.txt" $(FLUENT_BIT_BIN) -c fluent-bit.conf

run-gdb: $(FLUENT_BIT_BIN)
	gdb --args $(FLUENT_BIT_BIN) -c fluent-bit.conf

image:
	docker buildx build --build-arg FLUENT_BIT_VERSION=$(FLUENT_BIT_VERSION) \
		--label fluent-bit-version=$(FLUENT_BIT_VERSION) \
		-t avassa/fluent-bit:$(FLUENT_BIT_VERSION) .
