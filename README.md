# Fluent Bit input plugins

This repository provides two Fluent Bit input plugins `avassa_logs` and
`avassa_metrics`. Please see [Fluent Bit documentation](https://fluentbit.io/).

## Avassa Plugins
This plugins are intended to run in the app spec of an application to forward
logs and/or metrics to a third party. It serves as an [Fluent Bit input
plugin](https://docs.fluentbit.io/manual/concepts/data-pipeline/input).

It is supposed to run as a service with the application it monitors. It will
subscribe to the application's containers' stdout/stderr logs and feed those
logs into the Fluent Bit pipeline.

The metrics plugin can consume host, Edge Enforcer (supd), application and custom metrics.

### Policy and Application Role
Please create a policy as [policy.yml](./examples/policy.yml). This will allow
the plugin to read site name, list volga topics and access a vault named
`fluent-bit`.


**NOTE** If you change the name of the vault from where e.g. AWS or GCP credentials
are fetched from `fluent-bit`, the policy needs to be updated.


### Vaults
The policy and examples the credentials are stored in a vault called
`fluent-bit`. The name of the secret differs between the examples.

## Log input plugin
### Fluent Bit configuration for avassa_logs
Setting|Default|Description
---|---|---
interval_sec|15|The interval which Fluent Bit will poll for new logs
api_url|https://api:4646|URL to the Edge Enforcer URL, this should rarely be specified.
log_queue_len|100|Lenght of the internal log queue
approle_id| |Name of the application role
split_logs_at||Split incoming logs, if set to e.g. `\n` incoming multiline logs will be split into separated logs, one per line
application_re|Mandatory to set|Regexp to match the application name
container_re|Mandatory to set|Regexp to match containers in an application

You have to configure either application role or username/password login. **Strongly recommended to use app roles.**
If none of the below settings are set, the system assumes `APPROLE_SECRET_ID` is set in the environment.
Setting|Description
---|---
approle_id| Set the application role to use.
username| Username
password| Password


### Examples
For AWS CloudWatch, see [log-exporter-aws.app.yml](./examples/log-exporter-aws.app.yml).
For Google Stackdriver, see [log-exporter-stackdriver.app.yml](./examples/log-exporter-stackdriver.app.yml).

## Metrics input plugin
The metrics plugin consumes predefined metrics (application, host and supd) or custom metrics.

Setting|Default|Description
---|---|---
interval_sec|15|The interval which Fluent Bit will poll for new metrics
api_url|https://api:4646|URL to the Edge Enforcer URL, this should rarely be specified.
approle_id| |Name of the application role
enable_app_metrics| false | Collect application metrics
enable_host_metrics| false | Collect host metrics
enable_supd_metrics| false | Collect Edge Enforcer (supd) metrics
custom_metrics_config | | Path to custom metrics description file

### Custom metrics
The custom metrics allows for consuming metrics, in JSON format, from any Volga topic.

Create a YAML description file and mount it in the container, specify the path in `fluentbit.conf` 
using the `custom_metrics_config` setting.

The example below is for JSON payloads:
```json
{
  "reading": {
    "temperature": 2.01e+01,
    "humidity": 5.03e+01
  }
}
```

```yaml
topics:
  # Volga topic name
  - name: temp-hum
    metrics:
        # Fluent Bit metric name
      - name: temperature
        # JSON path to the value
        path: $.reading.temperature
        # Additional tags
        tags:
          sensor: mq21
      - name: humidity
        path: $.reading.humidity
        tags:
          sensor: mq22
```
**NOTE**: A `site` tag is always added with the site's name.

